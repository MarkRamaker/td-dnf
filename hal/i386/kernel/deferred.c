#include <hal/deferred.h>

#include "tty.h"
#include "gdt.h"
#include "interrupt.h"

/**************************************************************************************
 * @function: deferredInitialize
 * @in: struct multiboot_info *
 * @out: void
 * @comment: hal initialize function
 */
void deferredInitialize (struct multiboot_info * m_boot __attribute__((unused))) {
    terminalInitialize ();
    createBasicGdt ();
    initializeIgt (getGdtCodeSegementEntry ());
    activateIgt ();
}
