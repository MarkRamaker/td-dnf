#include "port.h"

/* File is temp, expecting more work for ports, and in c */

/**************************************************************************************
 * @function: writePort8BitSlow
 * @in: uint16_t, uint8_t
 * @out: void
 * @comment: implemented in asm
 */
void writePort8BitSlow (uint16_t port, uint8_t data) {
    __asm__ volatile("outb %0, %1\njmp 1f\n1: jmp 1f\n1:" : : "a" (data), "Nd" (port));
}

