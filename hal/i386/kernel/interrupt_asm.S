section .data
number DB 0 ; The Interrupt number will be stored here before jumping into the handler function.

section .text
extern interruptHandler 

%macro exHandler 1
global exHandler%1
exHandler%1:
    pusha
    mov byte [number], %1 ; Store Exception number.
    jmp interruptBegin
%endmacro

%macro irqHandler 1
global irqHandler%1
irqHandler%1:
    pusha
    mov byte [number], $1 + 0x20 ; Store Interrupt number.
    jmp interruptBegin
%endmacro

irqHandler 0x00
irqHandler 0x01

interruptBegin:
    push ds
    push es
    push fs
    push gs

    push esp
    push number
    call interruptHandler
    mov esp, eax ; Set the new stack pointer ATTENTION
    
    pop gs
    pop fs
    pop es
    pop ds
    popa

global interruptIgnore
interruptIgnore:
    iret
