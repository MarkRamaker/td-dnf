#ifndef INTERUPT_H
#define INTERUPT_H 1

#include <sys/types.h>

/**************************************************************************************
 * @struct: interruptGateEntry_t
 * @comment: packed
 */
struct interruptGateEntry_t {
    uint16_t handler_low;
    uint16_t code_segment_selector;
    uint8_t reserved;
    uint8_t access;
    uint16_t handler_high;
    
} __attribute__((packed));

/**************************************************************************************
 * @function: setInterruptGateEntry
 * @in: uint8_t, uint16_t, void (* ptr) (void), uint8_t, uint8_t
 * @out: void
 * @comment:
 */
void setInterruptGateEntry (
    uint8_t interrupt_number,
    uint16_t code_segment,
    void (* handler) (void),
    uint8_t privilage_level,
    uint8_t type
);

/**************************************************************************************
 * @function: initializeIgt
 * @in: uint16_t
 * @out: void
 * @comment
 */
void initializeIgt (uint16_t code_segment);

/**************************************************************************************
 * @function: activateIgt
 * @in: void
 * @out: void
 * @comment:
 */
void activateIgt (void);

/**************************************************************************************
 * @function: interruptHandler
 * @in: uint8_t, uint32_t
 * @out: uint32_t
 * @comment: called from asm, returns new esp
 */
uint32_t interruptHandler (uint8_t number, uint32_t esp);


#endif
