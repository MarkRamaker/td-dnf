#include "gdt.h"

#define UINT16_MAX 65536
#define MIB64 (64 * 1024 * 1024)

/**************************************************************************************
 * var: basic_table
 * @comment: packed
 */
static struct {
    struct gdt_entry_t null_segment;
    struct gdt_entry_t unused_segment;
    struct gdt_entry_t code_segment;
    struct gdt_entry_t data_segment;
} __attribute__((packed)) basic_table;

/**************************************************************************************
 * @function: setGdtEntry
 * @in: struct gdt_entry_t *, uint32_t, uint32_t, uint8_t
 * @out: void
 * @comment:
 */
void setGdtEntry (struct gdt_entry_t * entry, uint32_t base, uint32_t limit, uint8_t type) {
    if (limit < UINT16_MAX) {
        entry->flags = 0x4; // 16 bit entry.
    } else {
        entry->flags = 0xc; // 32 bit entry.
        
        // Virtualize the 12 lsbs.
        if ((limit & 0xfff) == 0xfff) {
            limit = (limit >> 12);
        } else {
            limit = (limit >> 12) - 1;
        }
    }
    
    // Set the low limit.
    entry->limit_low = (uint16_t) limit;
    
    // Set the high limit.
    entry->limit_high = (limit >> 16);
    
    // Set the low base.
    entry->base_low = (uint16_t) base;
    
    // Set the high base.
    entry->base_high = (base >> 16);
    
    // Set the type.
    entry->type = type;
}

/**************************************************************************************
 * @function: getGdtBase
 * @in: struct gdt_entry_t *
 * @out: uint32_t
 * @comment:
 */
uint32_t getGdtBase (struct gdt_entry_t * entry) {
    return (entry->base_high << 16) | entry->base_low;
}

/**************************************************************************************
 * @function: getGdtLimit
 * @in: struct gdt_entry_t *
 * @out: uint32_t
 * @comment:
 */
uint32_t getGdtLimit (struct gdt_entry_t * entry) {
    if (entry->flags & 0x4) {
        // 16 bit entry.
        return entry->limit_low;
        
    } else {
        // 32 bit entry with virtual bits.
        return (entry->limit_high << 28) | (entry->limit_low << 12) | 0xfff;
    }
}

/**************************************************************************************
 * @function: createBasicGdt
 * @in: void
 * @out: void
 * @comment:
 */
void createBasicGdt (void) {
    uint32_t loader [2];
    
    // Init a basic table.
    setGdtEntry (&(basic_table.null_segment), 0, 0, 0);
    setGdtEntry (&(basic_table.unused_segment), 0, 0, 0);
    setGdtEntry (&(basic_table.code_segment), 0, MIB64, 0x9a);
    setGdtEntry (&(basic_table.data_segment), 0, MIB64, 0x92);
    
    // Set loader values.
    loader [0] = sizeof (basic_table) << 16;
    loader [1] = (uint32_t) &basic_table;
    
    // Load the gdt with some asm.
    asm volatile("lgdt (%0)": :"p" (((uint8_t *) loader) + 2));
}

/**************************************************************************************
 * @function: getGdtAddress
 * @in: void
 * @out: void
 * @comment:
 */
uint32_t getGdtAddress (void) {
    return (uint32_t) &basic_table;
}

/**************************************************************************************
 * @function: getGdtCodeSegementEntry
 * @in: void
 * @out: void
 * @comment:
 */
uint16_t getGdtCodeSegementEntry (void) {
    return (uint32_t) &(basic_table.code_segment) - (uint32_t) &basic_table;
}

