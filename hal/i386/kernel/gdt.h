#ifndef GDT_H
#define GDT_H 1

#include <sys/types.h>

/**************************************************************************************
 * @struct: gdt_entry_t
 * @comment: packed
 */
struct gdt_entry_t {
    unsigned int limit_low   : 16;
    unsigned int base_low    : 24;
    unsigned int type        : 8;
    unsigned int limit_high  : 4;
    unsigned int flags       : 4;
    unsigned int base_high   : 8;
} __attribute__((packed));


/**************************************************************************************
 * @function: setGdtEntry
 * @in: struct gdt_entry_t *, uint32_t, uint32_t, uint8_t
 * @out: void
 * @comment:
 */
void setGdtEntry (struct gdt_entry_t * entry, uint32_t base, uint32_t limit, uint8_t type);

/**************************************************************************************
 * @function: getGdtBase
 * @in: struct gdt_entry_t *
 * @out: uint32_t
 * @comment:
 */
uint32_t getGdtBase (struct gdt_entry_t * entry);

/**************************************************************************************
 * @function: getGdtLimit
 * @in: struct gdt_entry_t *
 * @out: uint32_t
 * @comment:
 */
uint32_t getGdtLimit (struct gdt_entry_t * entry);

/**************************************************************************************
 * @function: createBasicGdt
 * @in: void
 * @out: void
 * @comment:
 */
void createBasicGdt (void);

/**************************************************************************************
 * @function: getGdtAddress
 * @in: void
 * @out: void
 * @comment:
 */
uint32_t getGdtAddress (void);

/**************************************************************************************
 * @function: getGdtCodeSegementEntry
 * @in: void
 * @out: void
 * @comment:
 */
uint16_t getGdtCodeSegementEntry (void);



#endif
