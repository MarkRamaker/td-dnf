#include "interrupt.h"
#include "port.h"

// FIXME
#include <lib/stdio.h>

#define IGT_SIZE 256

/**************************************************************************************
 * @var: static interrupt_gate_table
 * @comment: 
 */
static struct interruptGateEntry_t interrupt_gate_table [IGT_SIZE];

extern void interruptIgnore (void);
extern void irqHandler0x00 (void);
extern void irqHandler0x01 (void);
extern void irqHandler0x02 (void);
extern void irqHandler0x03 (void);
extern void irqHandler0x04 (void);
extern void irqHandler0x05 (void);
extern void irqHandler0x06 (void);
extern void irqHandler0x07 (void);

/**************************************************************************************
 * @function: setInterruptGateEntry                                                 
 * @in: uint8_t, uint16_t, void (* ptr) (void), uint8_t, uint8_t                    
 * @out: void                                                                       
 * @comment:                                                                        
 */
void setInterruptGateEntry (
    uint8_t interrupt_number,
    uint16_t code_segment,
    void (* handler) (void),
    uint8_t privilage_level,
    uint8_t type
) {
    const uint8_t IGT_DESC_PRESENT = 0x80;

    interrupt_gate_table [interrupt_number].handler_low = (uint32_t) handler & 0xffff;
    interrupt_gate_table [interrupt_number].handler_high = (uint32_t) handler >> 16 & 0xffff;

    interrupt_gate_table [interrupt_number].code_segment_selector = code_segment;

    interrupt_gate_table [interrupt_number].access = IGT_DESC_PRESENT | ((privilage_level & 3) << 5) |type ;
    
    interrupt_gate_table [interrupt_number].reserved = 0;
}

/**************************************************************************************
 * @function: initializeIgt                                                         
 * @in: uint16_t                                                                    
 * @out: void                                                                       
 * @comment:                                                                        
 */
void initializeIgt (uint16_t code_segment) {
    const uint8_t IGT_INTERRUPT_GATE = 0xe;
    
    for (uint32_t i = 0; i < IGT_SIZE; i++) {
        setInterruptGateEntry (i, code_segment, interruptIgnore, 0, IGT_INTERRUPT_GATE);
    }
    
    setInterruptGateEntry (0x20, code_segment, irqHandler0x00, 0, IGT_INTERRUPT_GATE);
    setInterruptGateEntry (0x21, code_segment, irqHandler0x01, 0, IGT_INTERRUPT_GATE);
    
    writePort8BitSlow (0x20, 0x11); // Master command: 0x20, data: 0x21.
    writePort8BitSlow (0xa0, 0x11); // Slave command: 0xa0, data: 0xa1.
    
    writePort8BitSlow (0x21, 0x20); // Master command: 0x20, data: 0x21.
    writePort8BitSlow (0xa1, 0x28); // Slave command: 0xa0, data: 0xa1.
    
    writePort8BitSlow (0x21, 0x04); // Master command: 0x20, data: 0x21.
    writePort8BitSlow (0xa1, 0x02); // Slave command: 0xa0, data: 0xa1.
    
    writePort8BitSlow (0x21, 0x01); // Master command: 0x20, data: 0x21.
    writePort8BitSlow (0xa1, 0x01); // Slave command: 0xa0, data: 0xa1.
    
    writePort8BitSlow (0x21, 0x00); // Master command: 0x20, data: 0x21.
    writePort8BitSlow (0xa1, 0x00); // Slave command: 0xa0, data: 0xa1.
    
    struct {
        uint16_t size;
        uint32_t base;
    } __attribute__((packed)) igtPointer = {
        .size = IGT_SIZE * sizeof (struct interruptGateEntry_t) - 1,
        .base = (uint32_t) &interrupt_gate_table
    };
    
    asm volatile ("lidt %0" : : "m" (igtPointer));
}

/**************************************************************************************
 * @function: activateIgt                                                         
 * @in: void                                                           
 * @out: void                                                                       
 * @comment:                                                                        
 */
void activateIgt (void) {
    asm ("sti");
}

/**************************************************************************************
 * @function: interruptHandler                                                         
 * @in: uint8_t, uint32_t                                                                    
 * @out: uint32_t
 * @comment: called from asm, returns new esp                                                                     
 */
uint32_t interruptHandler (uint8_t number __attribute__((unused)), uint32_t esp) {
    
    printf ("it works!!!!!!");
    return esp;
}
