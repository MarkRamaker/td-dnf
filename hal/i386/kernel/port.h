#ifndef PORT_H
#define PORT_H 1

#include <sys/types.h>

/**************************************************************************************
 * @function: writePort8Bit
 * @in: uint16_t, uint8_t
 * @out: void
 * @comment: implemented in asm
 */
void writePort8Bit (uint16_t port, uint8_t data);

/**************************************************************************************
 * @function: writePort8BitSlow
 * @in: uint16_t, uint8_t
 * @out: void
 * @comment: implemented in asm
 */
void writePort8BitSlow (uint16_t port, uint8_t data);

/**************************************************************************************
 * @function: writePort16Bit
 * @in: uint16_t, uint16_t
 * @out: void
 * @comment: implemented in asm
 */
void writePort16Bit (uint16_t port, uint16_t data);

/**************************************************************************************
 * @function: writePort32Bit
 * @in: uint16_t, uint32_t
 * @out: void
 * @comment: implemented in asm
 */
void writePort32Bit (uint16_t port, uint32_t data);





/**************************************************************************************
 * @function: readPort8Bit
 * @in: uint16_t
 * @out: void
 * @comment: implemented in asm
 */
uint8_t readPort8Bit (uint16_t port);

/**************************************************************************************
 * @function: readPort8Bit
 * @in: uint16_t
 * @out: void
 * @comment: implemented in asm
 */
uint16_t readPort16Bit (uint16_t port);

/**************************************************************************************
 * @function: readPort8Bit
 * @in: uint16_t
 * @out: void
 * @comment: implemented in asm
 */
uint32_t readPort32Bit (uint16_t port);

#endif
