# Default.
.DEFAULT: all

# Platform.
ARCH = i386

# Compilers and programs.
CC = gcc
AS = nasm
LD = ld
AR = ar

# Params.
CCPARAMS := -m32 -nostdlib -fno-builtin -fno-exceptions -Wall -Wextra
ASPARAMS :=
LDPARAMS :=

# Default build rule for c files.
%.o: %.c %.d
	$(CC) $(CCPARAMS) -o $@ -c $<
	
# Default build rule for S files.
%.o: %.S
	$(AS) $(ASPARAMS) $< -o $@
	
# Default build rule for dep files.
%.d: %.c
	$(CC) -MM $(CCPARAMS) -MT $(@:.d=.o) $< > $@
	
print-%  : ; @echo $* = $($*)
