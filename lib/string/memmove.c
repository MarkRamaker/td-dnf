#include <string.h>

void * memmove (void * destination, const void * source, size_t num) {
    if (destination == source || num == 0) return destination;
    if (destination > source) {
        // Start from the highest address.
        for (; num > 0; num--) {
            ((char *) destination) [num] = ((const char *) source) [num];
        }
        ((char *) destination) [num] = ((const char *) source) [num];
        
    } else {
        // Start from the lowest address.
        for (size_t i = 0; i < num; i++) {
            ((char *) destination) [i] = ((const char *) source) [i];
        }
    }
    return destination;
}
