#include <string.h>

void * memcpy (void * destination, const void * source, size_t num) {
    // Start from the lowest address.
    for (size_t i = 0; i < num; i++) {
        ((char *) destination) [i] = ((const char *) source) [i];
    }
    return destination;
}
