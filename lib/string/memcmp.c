#include <string.h>

int memcmp (const void * ptr1, const void * ptr2, size_t num) {
    for (size_t i = 0; i < num; i++) {
        if (((const char *) ptr1) [i] - ((const char *) ptr2) [i] != 0) return ((const char *) ptr1) [i] - ((const char *) ptr2) [i];
    }
    return 0;
}
