#include <string.h>

void * memset (void * restrict ptr, int value, size_t num) {
    for (size_t i = 0; i < num; i++) {
        ((char *) ptr) [i] = (char) value;
    }
    return ptr;
}
