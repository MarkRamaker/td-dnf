#include <stdio.h>
#include <stdlib.h>
 
__attribute__((__noreturn__))
void abort (void) {
#ifdef __is_libk
	// TODO: Add proper kernel panic.
	printf ("kernel: panic: abort()\n");
#else
	// TODO: Abnormally terminate the process as if by SIGABRT.
	printf ("abort()\n");
#endif
	for (;;);
	__builtin_unreachable ();
}
