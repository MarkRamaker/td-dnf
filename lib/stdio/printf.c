#include <stdio.h>
#include <sys/types.h>  
    
// FIXME.. should probably also set erno or something
#define CONDITIONAL_MACRO(x) \
    if (format_index > INT_MAX - x || print_count > INT_MAX - x) return print_count
 

/**************************************************************************************
 * @function: divmod
 * @in: unsigned long long, unsigned int, unsigned int *
 * @out: unsigned long long
 * @comment: borrowed from tyndur project
 */        
unsigned long long divmod (unsigned long long dividend, unsigned int divisor, unsigned int * remainder) {
    unsigned int highword = dividend >> 32;
    unsigned int lowword = dividend & 0xffffffff;
    unsigned long long quotient;
    unsigned int rem;

    __asm__("div %%ecx\n\t"
        "xchg %%ebx, %%eax\n\t"
        "div %%ecx\n\t"
        "xchg %%edx, %%ebx"
        : "=A"(quotient), "=b"(rem)
        : "a"(highword), "b"(lowword), "c"(divisor), "d"(0)
        );

    if(remainder)
    {
        *remainder = rem;
    }

    return quotient;
}

        
/**************************************************************************************
 * @function: printun
 * @in: unsigned long long, int, int, char, char, char
 * @out: int
 * @comment:
 */        
int printun (unsigned long long number, int radix, int pad_count, char pad_char, char left_align) {
    char b[65];
    char * r = b + 64;
    char digits [] = "0123456789abcdefghijklmnopqrstuvwxyz";
    unsigned int remainder;
    int print_count = 0;

    if (radix < 2 || radix > 36) {
        return 0;
    }

    *r-- = '\0';

    do
    {
        number = divmod (number, radix, &remainder);
        *r-- = digits [remainder];
        if (pad_count > 0 ) pad_count--;
    }
    while(number > 0);

    if (left_align) {
        for (r++; *r != '\0'; r++, print_count++) putchar ((int) *r); 
    }
    for (; pad_count > 0; pad_count--, print_count++) putchar ((int) pad_char);
    if (!left_align) {
        for (r++; *r != '\0'; r++, print_count++) putchar ((int) *r); 
    }

    return print_count;
}

    

/**************************************************************************************
 * @function: printf
 * @in: const char *, ...
 * @out: int
 * @comment:
 */
int printf (const char * format, ...) {
    char * subs;
    unsigned long long value = 0;
    int format_index = 0;
    int print_count = 0;
    unsigned int pad_count = 0;
    int precision = -1;
    char pad_char = ' ';
    char prepend_char __attribute__((unused)) = ' ';
    char left_align = 0;
    char * args = (char *) &format + sizeof (char *);
    
    for (; format_index < INT_MAX && print_count < INT_MAX && format [format_index] != '\0'; format_index++) {
        // Handle all normal prints.
        if (format [format_index] != '%') {
            putchar ((int) format [format_index]);
            CONDITIONAL_MACRO(1);
            print_count++;
            continue;
        }
        CONDITIONAL_MACRO(1);
        format_index++;
        
        // Handle %
        if (format [format_index] == '%') {
            putchar ((int) '%');
            CONDITIONAL_MACRO(1);
            print_count++;
            continue;
        }
        
        // Reset all kinds of fields.
        precision = -1;
        pad_count = 0;
        pad_char = ' ';
        prepend_char = ' ';
        left_align = 0;
        value = 0;
        
        // Handle flags field.
        while (format [format_index] == '-' || 
               format [format_index] == '+' || 
               format [format_index] == '0' || 
               format [format_index] == '#') {
            switch (format [format_index]) {
                case '-':
                    left_align = 1;
                    break;
                case '+':
                    prepend_char = '+';
                    break;
                case '0':
                    pad_char = '0';
                    break;
                case '#':
                    pad_char = '#';
                    break;
            }
            CONDITIONAL_MACRO(1);
            format_index++;
        }
        
        // Handle width field.
        if (format [format_index] == '*') {
            pad_count = *(int *) args;
            args += sizeof (int);
            CONDITIONAL_MACRO(1);
            format_index++;
        } else {
            while (format [format_index] >= '0' && format [format_index] <= '9') {
                pad_count *= 10;
                pad_count += format [format_index] - '0';
                CONDITIONAL_MACRO(1);
                format_index++;
            }
            pad_count &= INT_MAX;
        }
        
        // Handle precision field.
        if (format [format_index] == '.') {
            CONDITIONAL_MACRO(1);
            format_index++;
            if (format [format_index] == '*') {
                precision = *(int *) args & INT_MAX;
                args += sizeof (int);
                CONDITIONAL_MACRO(1);
                format_index++;
            } else {
                while (format [format_index] >= '0' && format [format_index] <= '9') {
                    precision *= 10;
                    precision += format [format_index] - '0';
                    CONDITIONAL_MACRO(1);
                    format_index++;
                }
                precision &= INT_MAX;
            }
        }
        
        // Handle length field.
        if (format [format_index] == 'l' &&
            format [format_index + 1] == 'l' &&
            (format [format_index + 2] == 'd' ||
            format [format_index + 2] == 'i' ||
            format [format_index + 2] == 'o' ||
            format [format_index + 2] == 'u' ||
            format [format_index + 2] == 'x')) {
            
            value = *(unsigned long long *) args;
            args += sizeof (unsigned long long);
            CONDITIONAL_MACRO(2);
            format_index += 2;
        } else if (format [format_index] == 'd' || 
                    format [format_index] == 'u') {
            value = *(int *) args;
            args += sizeof (int);
            if ((int) value < 0) {
                CONDITIONAL_MACRO(1);
                putchar ((int) '-');
                print_count++;
                if (pad_count > 0) pad_count--;
                value = -value;
            }
        } else if (format [format_index] == 'i' || 
                    format [format_index] == 'o' || 
                    format [format_index] == 'p' || 
                    format [format_index] == 'b' ||
                    format [format_index] == 'x') {
            value = *(int *) args;
            args += sizeof (int);
            value = value & 0xffffffff;
        }

        
        // Handle type field.
        switch (format [format_index]) {
            case '\0':
                return print_count;
            case 'c':
                CONDITIONAL_MACRO(1);
                putchar (*(int *) args);
                args += sizeof (int);
                print_count++;
                break;
            case 'd':
            case 'i':
            case 'u':
                CONDITIONAL_MACRO(64);
                print_count += printun (value, 10, pad_count, pad_char, left_align);
                break;
            case 'o':
                CONDITIONAL_MACRO(64);
                print_count += printun (value, 8, pad_count, pad_char, left_align);
                break;
            case 'b':
                CONDITIONAL_MACRO(64);
                print_count += printun (value, 2, pad_count, pad_char, left_align);
                break;
            case 'p':
            case 'x':
                CONDITIONAL_MACRO(64);
                print_count += printun (value, 16, pad_count, pad_char, left_align);
                break;
            case 's':
                subs = *(char **) args;
                args += sizeof (char *);
                for (; *subs != '\0' && precision != 0; subs++, precision--) {
                    CONDITIONAL_MACRO(1);
                    putchar ((int) *subs);
                    print_count++;
                }
                break;
            default:
                break;
        } 
    }
    return print_count;
}
