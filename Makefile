
# Include the build system wide definitions.
include Makefile.inc

# Rule definitions.
KHAL = hal/$(ARCH)/khal.a
LHAL = hal/$(ARCH)/lhal.a
SHAL = hal/$(ARCH)/shal.a
#HAL = $(KHAL) $(LHAL)
HAL = $(KHAL) $(LHAL) $(SHAL)
KERNEL = kernel/kernel.a
LIBK = lib/libk.a
LIBC = lib/libc.a
SYS = sys/sys.a
IMG = td-dnf.bin
ISO = td-dnf.iso

# Overwrite params.
LDPARAMS = -melf_i386

.PHONY: $(HAL) $(KERNEL) $(LIBK)

# All rule.
all: run

# HAL build rule.
$(HAL):
	$(MAKE) -C hal/$(ARCH)

# KERNEL build rule.
$(KERNEL):
	$(MAKE) -C kernel

# LIBK build rule.
$(LIBK):
	$(MAKE) -C lib $(@:lib/%=%)

# LIBC build rule.
$(LIBC):
	@echo "not finished yet"
	

# SYS build rule.
$(SYS):
	@echo "not finished yet"

# IMG build rule.
#$(IMG): $(KERNEL) $(KHAL) $(LHAL) $(LIBK)
$(IMG): $(KERNEL) $(HAL) $(LIBK)
	$(LD) $(LDPARAMS) -T linker/$(ARCH)/linker.ld -o $@ --start-group $^ --end-group

# ISO build rule.
.SILENT: $(ISO)
$(ISO): $(IMG)
	-mkdir -p iso/boot/grub
	-cp $< iso/boot/
	echo "set timeout=0" > iso/boot/grub/grub.cfg
	echo "set default=0" >> iso/boot/grub/grub.cfg
	echo "" >> iso/boot/grub/grub.cfg
	echo 'menuentry "td-dnf" {' >> iso/boot/grub/grub.cfg
	echo "	multiboot /boot/$(IMG)" >> iso/boot/grub/grub.cfg
	echo "}" >> iso/boot/grub/grub.cfg
	grub2-mkrescue --output=$@ iso &> /dev/null
	rm -rf iso

# Run the system.
.SILENT: run
run: $(ISO)
	-qemu-system-i386 -cdrom $< &> /dev/null &

.PHONY: clean
clean:
	-rm $(IMG) $(ISO)
	
.PHONY: clean-all
clean-all: clean
	$(MAKE) -C hal/$(ARCH) clean
	$(MAKE) -C lib clean
	$(MAKE) -C kernel clean
