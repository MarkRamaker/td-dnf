#include <hal/deferred.h>
#include <hal/multiboot.h>
#include <hal/tty.h>
#include <lib/stdio.h>

int k_main (struct multiboot_info * m_boot __attribute__((unused)), unsigned int magic __attribute__((unused))) {
    deferredInitialize (m_boot);
    printf ("Hello k world!\n");
    for (;;);
    return 0;
}
