#ifndef HAL_TTY_H
#define HAL_TTY_H 1

#include <sys/types.h>

void terminalWrite (const char * c, size_t size);
void terminalPutchar (char c);
void terminalWriteString (const char * data);

#endif
