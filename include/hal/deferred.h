#ifndef HAL_DEFERRED_H
#define HAL_DEFERRED_H 1

#include "multiboot.h"

void deferredInitialize (struct multiboot_info * m_boot);

#endif
