#ifndef LIBC_STDLIB_H
#define LIBC_STDLIB_H 1

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((__noreturn__))
void abort (void);

#ifdef __cplusplus
}
#endif

#endif
