#ifndef LIBC_STRING_H
#define LIBC_STRING_H 1

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t strlen (const char * str);
int memcmp (const void * ptr1, const void * ptr2, size_t num);
void * memcpy (void * destination, const void * source, size_t num);
void * memmove (void * destination, const void * source, size_t num);
void * memset (void * ptr, int value, size_t num);

#ifdef __cplusplus
}
#endif

#endif
