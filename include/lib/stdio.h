#ifndef LIBC_STDIO_H
#define LIBC_STDIO_H 1

#ifdef __cplusplus
extern "C" {
#endif

int printf (const char * format, ...);
int puts (const char * str);
int putchar (int ic);

#ifdef __cplusplus
}
#endif

#endif
